from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    # retrieve all instances of the "Project" model from the models.py file
    context = {"projects": projects}
    # defined for template to use queryset of this dict
    return render(request, "projects/project_list.html", context)
    # use imported shortcut render to render defined html
    # template will use context data


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    return render(
        request,
        "projects/project_detail.html",
        {"project": project, "tasks": tasks},
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
